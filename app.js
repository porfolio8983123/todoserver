const express = require('express');
const app = express();
const userRouter = require('./routes/userRoutes');
const cors = require('cors');
const taskRouter = require('./routes/taskRouter');
const mailRouter = require('./routes/mailRouter');

app.use(express.json());
app.use(cors());
app.use('/api',userRouter);
app.use('/api/task',taskRouter);
app.use('/api/mail',mailRouter);

module.exports = app;