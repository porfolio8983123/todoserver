const User = require('./../models/userModel');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

exports.signup = async (req,res) => {
    try {

        const {email,password,passwordConfirm} = req.body;

        if (password !== passwordConfirm) {
            return res.status(401).json({
                err:"Password doesn't match!"
            })
        }

        const user = await User.findOne({email});

        if (user) {
            return res.status(401).json({
                err:"User already exist!"
            })
        }

        const hashedPassword = await bcrypt.hash(password,12);

        const newUser = await User.create({
            ...req.body,
            password:hashedPassword
        });

        //token

        const token = jwt.sign({id: newUser._id},process.env.JWT_SECRET,{
            expiresIn: "10d"
        })

        res.status(200).json({
            status: 'success',
            message: 'User registered successfully',
            token,
            user:newUser
        })
    } catch (error) {
        res.status(500).json({
            err:error.message
        })
    }
}

exports.login = async (req,res,next) => {
    try {
        const {email,password} = req.body;
        console.log(email,password);
        
        if (!email || !password) {
            return res.status(401).json({
                err: "Please provide an email and password"
            })
        }

        const user = await User.findOne({email}).select('+password');

        if (!user) {
            return res.status(401).json({
                err: "User not found!"
            })
        }

        const isPasswordValid = await bcrypt.compare(password,user.password);

        if (!isPasswordValid) {
            return res.status(401).json({
                err: "Incorrect password or email!"
            })
        }

        const token = jwt.sign({id: user._id},process.env.JWT_SECRET,{
            expiresIn: "10d"
        })

        res.status(200).json({
            status: 'success',
            token,
            message: 'Logged in successfully!',
            user: {
                _id:user._id,
                name: user.name,
                email:user.email
            }
        })

    } catch (error) {
        res.status(500).json({err:error.message})
    }
}