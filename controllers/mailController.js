// password: wwbj mbde pvqp qgnt

const nodemailer = require('nodemailer');

exports.sendMail = async (req,res) => {
    try {

        const {email} = req.body;

        // Regular expression for email validation
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        // Check if the email matches the email format
        if (!emailRegex.test(email)) {
            return res.status(400).json({ err: 'Invalid email address' });
        }

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            auth: {
                user: "12200003.gcit@rub.edu.bt", //sender gmail
                pass: "wwbjmbdepvqpqgnt" //password
            }
        })
        
        const mailOptions = {
            from: {
                name: 'Todo',
                address: "12200003.gcit@rub.edu.bt"
            },
            to: email,
            subject: "Please join as a team!",
            html: `<a href = "https://lustrous-horse-e76656.netlify.app">https://lustrous-horse-e76656.netlify.app</a>`
        }

        await transporter.sendMail(mailOptions)
        res.status(200).json({'status':"success"})
    } catch (error) {
        res.status(500).json({err:error.message})
    }
}