const Task = require('../models/taskModel');

exports.addTask = async (req,res) => {
    try {

        const newTask = await Task.create(req.body)

        res.status(200).json({
            message:newTask
        })
    } catch (error) {
        res.status(500).json({
            err:error.message
        })
    }
}

exports.getTasks = async (req,res) => {
    try {
        const tasks = await Task.find();
        res.status(200).json({
            data:tasks
        })
    } catch (error) {
        res.status(500).json({
            err:error.message
        })
    }
}

exports.deleteTask = async (req,res) => {
    try {
        console.log(req.params.id);
        const task = await Task.findByIdAndDelete(req.params.id);
        res.json({data: task, status: 'success'});
    } catch (error) {
        res.status(500).json({err: error.message});
    }
}

exports.upDateImportant = async (req,res) => {
    try {
        const {id} = req.params;
        const {value} = req.body;

        console.log(value);

        const updatedUser = await Task.findByIdAndUpdate(
            id,
            { $set: { isImportant: value } }, 
            { new: true }
        );
        res.json({ data: updatedUser, status: 'success' });
    } catch (error) {
        res.status(500).json({err:error.message});
    }
}

exports.updateComplete = async (req,res) => {
    try {
        const {id} = req.params;
        const {value} = req.body;

        console.log(value);

        const updatedUser = await Task.findByIdAndUpdate(
            id,
            { $set: { isCompleted: value } }, 
            { new: true }
        );
        res.json({ data: updatedUser, status: 'success' });
    } catch (error) {
        res.status(500).json({err:error.message});
    }
}

exports.updateTask = async (req,res) => {
    try {
        const task = await Task.findByIdAndUpdate(req.params.id,req.body);
        res.json({data:task,status:'success'})
    } catch (error) {
        res.status(500).json({err:error.message})
    }
}