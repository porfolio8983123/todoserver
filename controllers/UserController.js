const User = require('../models/userModel');

exports.getAllUsers = async (req,res) => {
    try {
        const users = await User.find();
        res.status(200).json({
            data:users,
            status:'success'
        })
    } catch (error) {
        res.status(500).json({err:error.message})
    }
}

exports.deleteUser = async (req, res) => {
    try {
        // Extract the user ID from the request parameters
        const userId = req.params.id;

        // Find the user by ID and delete it
        const deletedUser = await User.findByIdAndDelete(userId);

        // If the user is successfully deleted, send a success response
        res.status(200).json({ message: 'User deleted successfully', deletedUser });
    } catch (error) {
        // If any error occurs during the deletion process, send a 500 status with the error message
        res.status(500).json({ error: error.message });
    }
};