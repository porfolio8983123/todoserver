const mongoose = require('mongoose');

const taskSchema = mongoose.Schema({
    taskName: {
        type: String,
        required:true
    },
    description: {
        type: String,
        required: true
    },
    dueDate: {
        type: Date,
        required:true
    },
    assignee: {
        type: String,
        default:"None"
    },
    isCompleted: {
        type: Boolean,
        default: false
    },
    isImportant: {
        type: Boolean,
        default: false
    },
    owner: {
        type:String,
        required: true
    }
})

const Task = mongoose.model('Task',taskSchema);
module.exports = Task;