const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController');

router 
    .route('/')
    .post(taskController.addTask)
    .get(taskController.getTasks)

router 
    .route('/:id')
    .delete(taskController.deleteTask)
    .patch(taskController.upDateImportant)
    .put(taskController.updateTask)

router.patch('/updateComplete/:id',taskController.updateComplete);
module.exports = router;