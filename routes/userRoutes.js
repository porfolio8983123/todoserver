const express = require('express');
const router = express.Router();
const userController = require('./../controllers/UserController');
const authController = require('./../controllers/authController');

router.post('/signup',authController.signup);
router.post('/login',authController.login);

router
    .route('/')
    .get(userController.getAllUsers)

router.delete('/:id',userController.deleteUser);

module.exports = router;